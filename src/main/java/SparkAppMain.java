import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;

import java.io.IOException;
import java.util.ArrayList;

public class SparkAppMain {
    public static void main(String[] args) throws IOException {
        SparkConf sparkConf = new SparkConf()
                .setAppName("Spark App")
                .setMaster("local[*]");
        JavaSparkContext sparkContext = new JavaSparkContext(sparkConf);

//        String path_input = "/usr/lib/sqoop/bin/citizens/part-m-00000";
//        String pathWrite = "ghj/output/";
        String path_input = args[0];
        String pathWrite = args[1];
        // create a Spark session
        SparkSession spark = SparkSession
                .builder()
                .appName("Java Spark SQL")
                .config("spark.some.config.option", "some-value")
                .getOrCreate();

        JavaRDD<Record> dataset_input = spark.read()
                .textFile(path_input)
                .javaRDD()
                .map(line -> {
                            String[] fields = line.split(",");
                            Record record = new Record();
                            record.setAge(Integer.parseInt(fields[0].trim()));
                            record.setSalary(Integer.parseInt(fields[3].trim()));
                            record.setTrips(Integer.parseInt(fields[4].trim()));
                            return record;
                        }
                );
        Dataset<Row> recordDF;
        // create DataFrame from RDD and set it as a temporary view
        recordDF = spark.createDataFrame(dataset_input, Record.class);
        recordDF.createOrReplaceTempView("record");

        // create DataSet, fill it and send to HDFS
        Dataset<String> sendDS = spark.createDataset(new ArrayList<String>(), Encoders.STRING());
        SalariesTripsAVG stAvg = new SalariesTripsAVG();
        stAvg.count_avg(recordDF, sendDS)
                .write()
                .format("text")
                .save(pathWrite);

        spark.stop();
    }
}