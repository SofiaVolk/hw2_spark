import java.io.Serializable;

public class Record implements Serializable {
    private int age;
    private int salary;
    private int trips;

    // get age method
    public int getAge() {
        return age;
    }

    // set age method
    public void setAge(int age) {
        this.age = age;
    }

    // get salary method
    public int getSalary() {
        return salary;
    }

    // set salary method
    public void setSalary(int salary) {
        this.salary = salary;
    }

    // get trips method
    public int getTrips() {
        return trips;
    }

    // set trips method
    public void setTrips(int trips) {
        this.trips = trips;
    }
}
