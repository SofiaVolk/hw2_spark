import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.functions;
import static org.apache.spark.sql.functions.col;

public class SalariesTripsAVG {

    public Dataset<String> count_avg(Dataset<Row> recordDF, Dataset<String> sendDS) {

        sendDS = recordDF.groupBy(col("age").$less(18),
                col("age").between(18, 60),
                col("age").gt(60))
                .agg(functions.avg("age"),
                        functions.avg("salary"),
                        functions.avg("trips"))
                .map(((MapFunction<Row, String>)
                        row -> "avgAge: " + row.get(3) + ", avgSalary: " + row.get(4) + ", avgTrips: " + row.get(5)),
                        Encoders.STRING());
        return sendDS;
    }
}
