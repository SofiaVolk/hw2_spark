## Cоздание исходной БД:
```
sudo docker pull mysql/mysql-server:latest
sudo docker run --name=mysql_bda1 -p 3306:3306 -d mysql/mysql-server:latest
sudo docker exec -it mysql_bda1 mysql -uroot -p
```
```
mysql> ALTER USER ‘usernameall’@’%t’ IDENTIFIED BY ‘ThePassword’;
mysql> grant all on *.* to 'usernameall'@'%';
mysql> Create database hw2;
mysql> use hw2;
mysql> create table `citizens` (`passport` INT, `age` INT, `month` TEXT, `salary` INT, `trips` INT);
mysql> insert into `citizens` VALUES (1, 2, "JAN", 100, 5), (100, 533, "JUN", 232, 23), (10, 53, "JUN", 23234, 3), (11, 63, "MAR", 32199, 5), (12, 34, "AUG", 55666, 9), (15, 10, "JUN", 2300, 6), (14, 73, "MAR", 321, 1), (13, 15, "AUG", 7566, 2);
```
## Импорт данных в hdfs:	
```
rm -r /usr/lib/sqoop/bin/citizens
sqoop import --connect jdbc:mysql://localhost:3306/hw2 --username usernameall --password ThePassword --table citizens --m 1 --bindir /usr/lib/sqoop/lib
```
## Чтение данных из hdfs:
```
hdfs dfs -text /usr/lib/sqoop/bin/citizens/part-m-00000
```
## Сборка проекта:
```
mvn clean install
```
## Запуск:
```
spark-submit --class SparkAppMain --master local target/ghj-1.0-SNAPSHOT.jar hdfs://localhost:41327/usr/lib/sqoop/bin/citizens/part-m-00000 hdfs://localhost:41327/usr/lib/sqoop/bin/citizens/output
```